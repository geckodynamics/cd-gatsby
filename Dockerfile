FROM node:14

# Update repository indexes
RUN apt-get update

# Install required libraries
RUN apt-get install libglu1 libvips musl-dev -y

# Clean apt cache
RUN apt-get clean
